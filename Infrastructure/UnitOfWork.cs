﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Infrastructure.Repositories;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext context;
        private IUsersRepository usersRepository;
        private ILicenceRepository licenceRepository;
        private IDepartmentRepository departmentRepository;
        private ILicenceTrackingRepository licenceTrackingRepository;
        public UnitOfWork(AppDbContext context)
        {
            this.context = context;
        }

        public ILicenceRepository LicenceRepository
        {
            get { return licenceRepository ??= new LicenceRepository(context); }
        }
        public IDepartmentRepository DepartmentRepository
        {
            get { return departmentRepository ??= new DepartmentRepository(context); }
        }
        public IUsersRepository UsersRepository
        {
            get { return usersRepository ??= new UserRepository(context); }
        }
        public ILicenceTrackingRepository LicenceTrackingRepository
        {
            get { return licenceTrackingRepository ??= new LicenceTrackingRepository(context); }
        }

        public void Save()
        {
            context.SaveChanges();
        }
        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}
