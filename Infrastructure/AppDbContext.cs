﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CertificateTitleType> CertificateTitleTypes { get; set; }
        public virtual DbSet<ConclusionOfAuthorizedStateBody> ConclusionOfAuthorizedStateBodies { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<DuplicateReissueReason> DuplicateReissueReasons { get; set; }
        public virtual DbSet<EducationalPrograms> EducationalPrograms { get; set; }
        public virtual DbSet<ImpactMeasures> ImpactMeasures { get; set; }
        public virtual DbSet<InstitutionalLegalForm> InstitutionalLegalForms { get; set; }
        public virtual DbSet<Licence> Licences { get; set; }
        public virtual DbSet<LicenceAcceptance> LicenceAcceptances { get; set; }
        public virtual DbSet<LicenceCancelationReason> LicenceCancelationReasons { get; set; }
        public virtual DbSet<LicenceDenialReason> LicenceDenialReasons { get; set; }
        public virtual DbSet<LicenceForm> LicenceForms { get; set; }
        public virtual DbSet<LicencePeriod> LicencePeriods { get; set; }
        public virtual DbSet<LicenceProcesses> LicenceProcesses { get; set; }
        public virtual DbSet<LicenceReissueReason> LicenceReissueReasons { get; set; }
        public virtual DbSet<LicenceRenewal> LicenceRenewals { get; set; }
        public virtual DbSet<LicenceType> LicenceTypes { get; set; }
        public virtual DbSet<TerritoryLocation> TerritoryLocations { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<LicenceTracking> LicenceTrackings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LicenceType>().HasData(
                new LicenceType() { Id = 1, Name = "Медицинская деятельность" },
                new LicenceType() { Id = 2, Name = "Фармацевтическая деятельность" },
                new LicenceType() { Id = 3, Name = "Образовательная деятельность" },
                new LicenceType() { Id = 4, Name = "Работа с микроорганизмами" });

            modelBuilder.Entity<EducationalPrograms>().HasData(
                new EducationalPrograms() { Id = 1, Name = "Общеобразовательные" },
                new EducationalPrograms() { Id = 2, Name = "Профессиональные" },
                new EducationalPrograms() { Id = 3, Name = "Послевузовское профессиональное образование" },
                new EducationalPrograms() { Id = 4, Name = "Дополнительное профессиональное образование" },
                new EducationalPrograms() { Id = 5, Name = "Дополнительное образование" });

            modelBuilder.Entity<ConclusionOfAuthorizedStateBody>().HasData(
                new ConclusionOfAuthorizedStateBody()
                {
                    Id = 1,
                    Body =
                        "Заключение уполномоченного государственного органа в сфере обеспечения безопасности дорожного движения"
                },
                new ConclusionOfAuthorizedStateBody()
                    {Id = 2, Body = "Заключение уполномоченного государственного органа в сфере здравоохранения"},
                new ConclusionOfAuthorizedStateBody()
                {
                    Id = 3,
                    Body =
                        "Заключение уполномоченного государственного органа по делам религий на образовательную деятельность религиозных организаций"
                });

            modelBuilder.Entity<LicenceProcesses>().HasData(
                new LicenceProcesses() { Id = 1, Name = "Выдача лицензии" },
                new LicenceProcesses() { Id = 2, Name = "Отказ о выдаче лицензии" },
                new LicenceProcesses() { Id = 3, Name = "Переоформление лицензии" },
                new LicenceProcesses() { Id = 4, Name = "Выдача дубликата лицензии" },
                new LicenceProcesses() { Id = 5, Name = "Приостановление" },
                new LicenceProcesses() { Id = 6, Name = "Возобновление" },
                new LicenceProcesses() { Id = 7, Name = "Анулирование" },
                new LicenceProcesses() { Id = 8, Name = "Признание" });

            modelBuilder.Entity<TerritoryLocation>().HasData(
                new TerritoryLocation() { Id = 1, Name = "Бишкек" },
                new TerritoryLocation() { Id = 2, Name = "Ош" },
                new TerritoryLocation() { Id = 3, Name = "Талас" },
                new TerritoryLocation() { Id = 4, Name = "Джалал Абад" },
                new TerritoryLocation() { Id = 5, Name = "Признание" });

            modelBuilder.Entity<Country>().HasData(
                new Country() { Id = 1, Name = "Россия" },
                new Country() { Id = 2, Name = "Кыргызстан" },
                new Country() { Id = 3, Name = "Казахстан" },
                new Country() { Id = 4, Name = "США" },
                new Country() { Id = 5, Name = "Япония" });

            modelBuilder.Entity<LicenceForm>().HasData(
                new LicenceForm() { Id = 1, Name = "Бумажная форма лицензии" },
                new LicenceForm() { Id = 2, Name = "Электронная форма лицензии" });

            modelBuilder.Entity<InstitutionalLegalForm>().HasData(
                new InstitutionalLegalForm() { Id = 1, Title = "Открытое акционерное общество" },
                new InstitutionalLegalForm() { Id = 2, Title = "Закрытое акционерное общество" },
                new InstitutionalLegalForm() { Id = 3, Title = "Общество с ограниченной ответственностью" },
                new InstitutionalLegalForm() { Id = 4, Title = "Общество с дополнительной ответственностью" },
                new InstitutionalLegalForm() { Id = 5, Title = "Коммандитное товарищество " },
                new InstitutionalLegalForm() { Id = 6, Title = "Полное товарищество" },
                new InstitutionalLegalForm() { Id = 7, Title = "Кооператив как коммерческая организация" });

            modelBuilder.Entity<LicencePeriod>().HasData(
                new LicencePeriod() { Id = 1, Name = "Безсрочная" },
                new LicencePeriod() { Id = 2, Name = "Временная" });

            modelBuilder.Entity<LicenceDenialReason>().HasData(
                new LicenceDenialReason() { Id = 1, Name = "Наличие запрета" },
                new LicenceDenialReason() { Id = 2, Name = "Наличие решения суда" },
                new LicenceDenialReason() { Id = 3, Name = "Представление заявителем недостоверной или неполной информации" },
                new LicenceDenialReason() { Id = 4, Name = "Несоответствие документов, представленных заявителем к  требованиям, установленным законодательством" },
                new LicenceDenialReason() { Id = 5, Name = "Невнесение лицензионного сбора" });

            modelBuilder.Entity<LicenceReissueReason>().HasData(
                new LicenceReissueReason() { Id = 1, Name = "Реорганизация юридического лица" },
                new LicenceReissueReason() { Id = 2, Name = "Изменение наименования юридического лица" },
                new LicenceReissueReason() { Id = 3, Name = "Изменение фамилии, имени, отчества физического лица" });

            modelBuilder.Entity<DuplicateReissueReason>().HasData(
                new DuplicateReissueReason() { Id = 1, Name = "Утеря" },
                new DuplicateReissueReason() { Id = 2, Name = "Порча" });

            modelBuilder.Entity<LicenceRenewal>().HasData(
                new LicenceRenewal() { Id = 1, ReasonName = "Возобновление", LicenceRenewalPeriod = 20},
                new LicenceRenewal() { Id = 2, ReasonName = "Другое", LicenceRenewalPeriod = 50 });


            modelBuilder.Entity<LicenceCancelationReason>().HasData(
                new LicenceCancelationReason() { Id = 1, Name = "Официальное уведомления лицензиатом о ликвидации юридического лица" },
                new LicenceCancelationReason() { Id = 2, Name = "Вступления в законную силу решения суда об аннулировании лицензии" },
                new LicenceCancelationReason() { Id = 3, Name = "Подачи заявления лицензиатом" });

            modelBuilder.Entity<LicenceAcceptance>().HasData(
                new LicenceAcceptance() { Id = 1, Name = "Официальное уведомления лицензиатом о ликвидации юридического лица", CountryAgencyNameId = null, CountryLicensorId = null},
                new LicenceAcceptance() { Id = 2, Name = "Вступления в законную силу решения суда об аннулировании лицензии", CountryAgencyNameId = null, CountryLicensorId = null },
                new LicenceAcceptance() { Id = 3, Name = "Подачи заявления лицензиатом", CountryAgencyNameId = null, CountryLicensorId = null });

            modelBuilder.Entity<ImpactMeasures>().HasData(
                new ImpactMeasures() { Id = 1, Type = "Предупреждение" },
                new ImpactMeasures() { Id = 2, Type = "Штраф" },
                new ImpactMeasures() { Id = 3, Type = "Приостановление действия лицензии и (или) разрешения" },
                new ImpactMeasures() { Id = 4, Type = "Подача искового заявления в судебные органы для рассмотрения вопроса об аннулировании лицензии и (или) разрешения" },
                new ImpactMeasures() { Id = 5, Type = "Очередность нарушения лицензионных требований" });

            modelBuilder.Entity<LicenceAcceptance>()
                .HasOne(x => x.CountryAgencyName)
                .WithMany(z => z.LicenceAcceptanceAgency)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<LicenceAcceptance>()
                .HasOne(x => x.CountryLicensor)
                .WithMany(z => z.LicenceAcceptanceLicencor)
                .OnDelete(DeleteBehavior.NoAction);

            base.OnModelCreating(modelBuilder);
        }
    }
}
