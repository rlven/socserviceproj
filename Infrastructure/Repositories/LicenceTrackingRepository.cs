﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    class LicenceTrackingRepository : Repository<LicenceTracking, int>, ILicenceTrackingRepository
    {
        public LicenceTrackingRepository(AppDbContext context) : base(context)
        {

        }
    }
}
