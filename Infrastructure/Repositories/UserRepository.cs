﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class UserRepository : Repository<User, string>, IUsersRepository
    {
        public UserRepository(AppDbContext context) : base(context)
        {

        }
    }
}
