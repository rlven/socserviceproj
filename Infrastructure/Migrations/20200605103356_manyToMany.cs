﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class manyToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_LicenceTrackings_LicenceId",
                table: "LicenceTrackings");

            migrationBuilder.DropIndex(
                name: "IX_LicenceTrackings_UserId",
                table: "LicenceTrackings");

            migrationBuilder.CreateIndex(
                name: "IX_LicenceTrackings_LicenceId",
                table: "LicenceTrackings",
                column: "LicenceId");

            migrationBuilder.CreateIndex(
                name: "IX_LicenceTrackings_UserId",
                table: "LicenceTrackings",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_LicenceTrackings_LicenceId",
                table: "LicenceTrackings");

            migrationBuilder.DropIndex(
                name: "IX_LicenceTrackings_UserId",
                table: "LicenceTrackings");

            migrationBuilder.CreateIndex(
                name: "IX_LicenceTrackings_LicenceId",
                table: "LicenceTrackings",
                column: "LicenceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LicenceTrackings_UserId",
                table: "LicenceTrackings",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");
        }
    }
}
