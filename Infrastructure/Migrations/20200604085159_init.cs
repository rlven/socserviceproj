﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CertificateTitleTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TitleId = table.Column<string>(nullable: true),
                    TitleType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CertificateTitleTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConclusionOfAuthorizedStateBodies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Body = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConclusionOfAuthorizedStateBodies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DuplicateReissueReasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DuplicateReissueReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EducationalPrograms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EducationalPrograms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ImpactMeasures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImpactMeasures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InstitutionalLegalForms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstitutionalLegalForms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LicenceDenialReasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceDenialReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LicenceForms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceForms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LicencePeriods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicencePeriods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LicenceProcesses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceProcesses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LicenceReissueReasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceReissueReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LicenceRenewals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReasonName = table.Column<string>(nullable: true),
                    LicenceRenewalPeriod = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceRenewals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LicenceTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TerritoryLocations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TerritoryLocations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LicenceAcceptances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    CountryLicensorId = table.Column<int>(nullable: false),
                    CountryAgencyNameId = table.Column<int>(nullable: false),
                    ForeignLicenceTermReview = table.Column<string>(nullable: true),
                    ForeignLicencePeriod = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceAcceptances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LicenceAcceptances_Countries_CountryAgencyNameId",
                        column: x => x.CountryAgencyNameId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_LicenceAcceptances_Countries_CountryLicensorId",
                        column: x => x.CountryLicensorId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FatherName = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    Signature = table.Column<string>(nullable: true),
                    DepartmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Licences",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LicenceNumber = table.Column<int>(nullable: false),
                    TitleKg = table.Column<string>(nullable: true),
                    TitleRu = table.Column<string>(nullable: true),
                    TitleEn = table.Column<string>(nullable: true),
                    TitleShort = table.Column<string>(nullable: true),
                    LegalAddress = table.Column<string>(nullable: true),
                    ActualAddress = table.Column<string>(nullable: true),
                    TextpayerIdentityNumber = table.Column<int>(nullable: false),
                    PhoneNumber = table.Column<int>(nullable: false),
                    EmailAddress = table.Column<string>(nullable: true),
                    CharterNumber = table.Column<string>(nullable: true),
                    StateRegCertificate = table.Column<string>(nullable: true),
                    LicenceDenialPeriod = table.Column<int>(nullable: false),
                    LicenceDenialPeriodAuto = table.Column<int>(nullable: false),
                    LicenceReissueReasonPeriod = table.Column<int>(nullable: false),
                    LicenceDulicateIssuePeriod = table.Column<int>(nullable: false),
                    IssueDate = table.Column<DateTime>(nullable: false),
                    DuplicateIssueDate = table.Column<DateTime>(nullable: false),
                    ReissueDate = table.Column<DateTime>(nullable: false),
                    LicenceAcceptanceDate = table.Column<DateTime>(nullable: false),
                    LicenceSuspensionDate = table.Column<DateTime>(nullable: false),
                    LicenceRenewalDate = table.Column<DateTime>(nullable: false),
                    LicenceCancellationDate = table.Column<DateTime>(nullable: false),
                    LicenceTypeId = table.Column<int>(nullable: false),
                    EducationalProgramsId = table.Column<int>(nullable: false),
                    ConclusionOfAuthorizedStateBodyId = table.Column<int>(nullable: false),
                    LicenceProcessesId = table.Column<int>(nullable: false),
                    LicencePeriodId = table.Column<int>(nullable: false),
                    InstitutionalLegalFormId = table.Column<int>(nullable: false),
                    LicenceFormId = table.Column<int>(nullable: false),
                    TerritoryLocationId = table.Column<int>(nullable: false),
                    LicenceDenialReasonId = table.Column<int>(nullable: false),
                    LicenceReissueReasonId = table.Column<int>(nullable: false),
                    DuplicateReissueReasonId = table.Column<int>(nullable: false),
                    LicenceRenewalId = table.Column<int>(nullable: false),
                    ImpactMeasuresId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    LicenceAcceptanceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Licences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Licences_ConclusionOfAuthorizedStateBodies_ConclusionOfAuthorizedStateBodyId",
                        column: x => x.ConclusionOfAuthorizedStateBodyId,
                        principalTable: "ConclusionOfAuthorizedStateBodies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_DuplicateReissueReasons_DuplicateReissueReasonId",
                        column: x => x.DuplicateReissueReasonId,
                        principalTable: "DuplicateReissueReasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_EducationalPrograms_EducationalProgramsId",
                        column: x => x.EducationalProgramsId,
                        principalTable: "EducationalPrograms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_ImpactMeasures_ImpactMeasuresId",
                        column: x => x.ImpactMeasuresId,
                        principalTable: "ImpactMeasures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_InstitutionalLegalForms_InstitutionalLegalFormId",
                        column: x => x.InstitutionalLegalFormId,
                        principalTable: "InstitutionalLegalForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicenceAcceptances_LicenceAcceptanceId",
                        column: x => x.LicenceAcceptanceId,
                        principalTable: "LicenceAcceptances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicenceDenialReasons_LicenceDenialReasonId",
                        column: x => x.LicenceDenialReasonId,
                        principalTable: "LicenceDenialReasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicenceForms_LicenceFormId",
                        column: x => x.LicenceFormId,
                        principalTable: "LicenceForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicencePeriods_LicencePeriodId",
                        column: x => x.LicencePeriodId,
                        principalTable: "LicencePeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicenceProcesses_LicenceProcessesId",
                        column: x => x.LicenceProcessesId,
                        principalTable: "LicenceProcesses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicenceReissueReasons_LicenceReissueReasonId",
                        column: x => x.LicenceReissueReasonId,
                        principalTable: "LicenceReissueReasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicenceRenewals_LicenceRenewalId",
                        column: x => x.LicenceRenewalId,
                        principalTable: "LicenceRenewals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_LicenceTypes_LicenceTypeId",
                        column: x => x.LicenceTypeId,
                        principalTable: "LicenceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Licences_TerritoryLocations_TerritoryLocationId",
                        column: x => x.TerritoryLocationId,
                        principalTable: "TerritoryLocations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LicenceCancelationReasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    LicenceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LicenceCancelationReasons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LicenceCancelationReasons_Licences_LicenceId",
                        column: x => x.LicenceId,
                        principalTable: "Licences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "ConclusionOfAuthorizedStateBodies",
                columns: new[] { "Id", "Body" },
                values: new object[,]
                {
                    { 1, "Заключение уполномоченного государственного органа в сфере обеспечения безопасности дорожного движения" },
                    { 2, "Заключение уполномоченного государственного органа в сфере здравоохранения" },
                    { 3, "Заключение уполномоченного государственного органа по делам религий на образовательную деятельность религиозных организаций" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Россия" },
                    { 2, "Кыргызстан" },
                    { 3, "Казахстан" },
                    { 4, "США" },
                    { 5, "Япония" }
                });

            migrationBuilder.InsertData(
                table: "DuplicateReissueReasons",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 2, "Порча" },
                    { 1, "Утеря" }
                });

            migrationBuilder.InsertData(
                table: "EducationalPrograms",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Общеобразовательные" },
                    { 2, "Профессиональные" },
                    { 3, "Послевузовское профессиональное образование" },
                    { 4, "Дополнительное профессиональное образование" },
                    { 5, "Дополнительное образование" }
                });

            migrationBuilder.InsertData(
                table: "ImpactMeasures",
                columns: new[] { "Id", "Type" },
                values: new object[,]
                {
                    { 5, "Очередность нарушения лицензионных требований" },
                    { 4, "Подача искового заявления в судебные органы для рассмотрения вопроса об аннулировании лицензии и (или) разрешения" },
                    { 1, "Предупреждение" },
                    { 2, "Штраф" },
                    { 3, "Приостановление действия лицензии и (или) разрешения" }
                });

            migrationBuilder.InsertData(
                table: "InstitutionalLegalForms",
                columns: new[] { "Id", "Title" },
                values: new object[,]
                {
                    { 1, "Открытое акционерное общество" },
                    { 2, "Закрытое акционерное общество" },
                    { 3, "Общество с ограниченной ответственностью" },
                    { 4, "Общество с дополнительной ответственностью" },
                    { 5, "Коммандитное товарищество " },
                    { 6, "Полное товарищество" },
                    { 7, "Кооператив как коммерческая организация" }
                });

            migrationBuilder.InsertData(
                table: "LicenceCancelationReasons",
                columns: new[] { "Id", "LicenceId", "Name" },
                values: new object[,]
                {
                    { 1, null, "Официальное уведомления лицензиатом о ликвидации юридического лица" },
                    { 2, null, "Вступления в законную силу решения суда об аннулировании лицензии" },
                    { 3, null, "Подачи заявления лицензиатом" }
                });

            migrationBuilder.InsertData(
                table: "LicenceDenialReasons",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 5, "Невнесение лицензионного сбора" },
                    { 4, "Несоответствие документов, представленных заявителем к  требованиям, установленным законодательством" },
                    { 1, "Наличие запрета" },
                    { 2, "Наличие решения суда" },
                    { 3, "Представление заявителем недостоверной или неполной информации" }
                });

            migrationBuilder.InsertData(
                table: "LicenceForms",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Бумажная форма лицензии" },
                    { 2, "Электронная форма лицензии" }
                });

            migrationBuilder.InsertData(
                table: "LicencePeriods",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Безсрочная" },
                    { 2, "Временная" }
                });

            migrationBuilder.InsertData(
                table: "LicenceProcesses",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 7, "Анулирование" },
                    { 6, "Возобновление" },
                    { 5, "Приостановление" },
                    { 8, "Признание" },
                    { 3, "Переоформление лицензии" },
                    { 2, "Отказ о выдаче лицензии" },
                    { 1, "Выдача лицензии" },
                    { 4, "Выдача дубликата лицензии" }
                });

            migrationBuilder.InsertData(
                table: "LicenceReissueReasons",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Реорганизация юридического лица" },
                    { 2, "Изменение наименования юридического лица" },
                    { 3, "Изменение фамилии, имени, отчества физического лица" }
                });

            migrationBuilder.InsertData(
                table: "LicenceRenewals",
                columns: new[] { "Id", "LicenceRenewalPeriod", "ReasonName" },
                values: new object[,]
                {
                    { 2, 50, "Другое" },
                    { 1, 20, "Возобновление" }
                });

            migrationBuilder.InsertData(
                table: "LicenceTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Медицинская деятельность" },
                    { 2, "Фармацевтическая деятельность" },
                    { 3, "Образовательная деятельность" },
                    { 4, "Работа с микроорганизмами" }
                });

            migrationBuilder.InsertData(
                table: "TerritoryLocations",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 4, "Джалал Абад" },
                    { 1, "Бишкек" },
                    { 2, "Ош" },
                    { 3, "Талас" },
                    { 5, "Признание" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_DepartmentId",
                table: "AspNetUsers",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_LicenceAcceptances_CountryAgencyNameId",
                table: "LicenceAcceptances",
                column: "CountryAgencyNameId");

            migrationBuilder.CreateIndex(
                name: "IX_LicenceAcceptances_CountryLicensorId",
                table: "LicenceAcceptances",
                column: "CountryLicensorId");

            migrationBuilder.CreateIndex(
                name: "IX_LicenceCancelationReasons_LicenceId",
                table: "LicenceCancelationReasons",
                column: "LicenceId");

            migrationBuilder.CreateIndex(
                name: "IX_Licences_ConclusionOfAuthorizedStateBodyId",
                table: "Licences",
                column: "ConclusionOfAuthorizedStateBodyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_DepartmentId",
                table: "Licences",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Licences_DuplicateReissueReasonId",
                table: "Licences",
                column: "DuplicateReissueReasonId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_EducationalProgramsId",
                table: "Licences",
                column: "EducationalProgramsId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_ImpactMeasuresId",
                table: "Licences",
                column: "ImpactMeasuresId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_InstitutionalLegalFormId",
                table: "Licences",
                column: "InstitutionalLegalFormId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicenceAcceptanceId",
                table: "Licences",
                column: "LicenceAcceptanceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicenceDenialReasonId",
                table: "Licences",
                column: "LicenceDenialReasonId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicenceFormId",
                table: "Licences",
                column: "LicenceFormId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicencePeriodId",
                table: "Licences",
                column: "LicencePeriodId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicenceProcessesId",
                table: "Licences",
                column: "LicenceProcessesId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicenceReissueReasonId",
                table: "Licences",
                column: "LicenceReissueReasonId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicenceRenewalId",
                table: "Licences",
                column: "LicenceRenewalId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_LicenceTypeId",
                table: "Licences",
                column: "LicenceTypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Licences_TerritoryLocationId",
                table: "Licences",
                column: "TerritoryLocationId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CertificateTitleTypes");

            migrationBuilder.DropTable(
                name: "LicenceCancelationReasons");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Licences");

            migrationBuilder.DropTable(
                name: "ConclusionOfAuthorizedStateBodies");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "DuplicateReissueReasons");

            migrationBuilder.DropTable(
                name: "EducationalPrograms");

            migrationBuilder.DropTable(
                name: "ImpactMeasures");

            migrationBuilder.DropTable(
                name: "InstitutionalLegalForms");

            migrationBuilder.DropTable(
                name: "LicenceAcceptances");

            migrationBuilder.DropTable(
                name: "LicenceDenialReasons");

            migrationBuilder.DropTable(
                name: "LicenceForms");

            migrationBuilder.DropTable(
                name: "LicencePeriods");

            migrationBuilder.DropTable(
                name: "LicenceProcesses");

            migrationBuilder.DropTable(
                name: "LicenceReissueReasons");

            migrationBuilder.DropTable(
                name: "LicenceRenewals");

            migrationBuilder.DropTable(
                name: "LicenceTypes");

            migrationBuilder.DropTable(
                name: "TerritoryLocations");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
