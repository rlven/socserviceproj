﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class rmvkey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CountryLicensorId",
                table: "LicenceAcceptances",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "CountryAgencyNameId",
                table: "LicenceAcceptances",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.InsertData(
                table: "LicenceAcceptances",
                columns: new[] { "Id", "CountryAgencyNameId", "CountryLicensorId", "ForeignLicencePeriod", "ForeignLicenceTermReview", "Name" },
                values: new object[] { 1, null, null, null, null, "Официальное уведомления лицензиатом о ликвидации юридического лица" });

            migrationBuilder.InsertData(
                table: "LicenceAcceptances",
                columns: new[] { "Id", "CountryAgencyNameId", "CountryLicensorId", "ForeignLicencePeriod", "ForeignLicenceTermReview", "Name" },
                values: new object[] { 2, null, null, null, null, "Вступления в законную силу решения суда об аннулировании лицензии" });

            migrationBuilder.InsertData(
                table: "LicenceAcceptances",
                columns: new[] { "Id", "CountryAgencyNameId", "CountryLicensorId", "ForeignLicencePeriod", "ForeignLicenceTermReview", "Name" },
                values: new object[] { 3, null, null, null, null, "Подачи заявления лицензиатом" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "LicenceAcceptances",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "LicenceAcceptances",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "LicenceAcceptances",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.AlterColumn<int>(
                name: "CountryLicensorId",
                table: "LicenceAcceptances",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryAgencyNameId",
                table: "LicenceAcceptances",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
