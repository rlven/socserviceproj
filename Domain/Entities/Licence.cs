﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Licence : Entity<int>
    {
        public int LicenceNumber { get; set; }
        public string TitleKg { get; set; }
        public string TitleRu { get; set; }
        public string TitleEn { get; set; }
        public string TitleShort { get; set; }
        public string LegalAddress { get; set; }
        public string ActualAddress { get; set; }
        public int TextpayerIdentityNumber { get; set; }
        public int PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string CharterNumber { get; set; }
        public string StateRegCertificate { get; set; }
        public int LicenceDenialPeriod { get; set; }
        public int LicenceDenialPeriodAuto { get; set; }
        public int LicenceReissueReasonPeriod { get; set; }
        public int LicenceDulicateIssuePeriod { get; set; }

        public DateTime IssueDate { get; set; }
        public DateTime DuplicateIssueDate { get; set; }
        public DateTime ReissueDate { get; set; }
        public DateTime LicenceAcceptanceDate { get; set; }
        public DateTime LicenceSuspensionDate { get; set; }
        public DateTime LicenceRenewalDate { get; set; }
        public DateTime LicenceCancellationDate { get; set; }
        public virtual LicenceType LicenceType { get; set; }
        [ForeignKey("LicenceType")]
        public int LicenceTypeId { get; set; }
        public virtual EducationalPrograms EducationalPrograms { get; set; }
        [ForeignKey("EducationalPrograms")]
        public int EducationalProgramsId { get; set; }
        public virtual ConclusionOfAuthorizedStateBody ConclusionOfAuthorizedStateBody { get; set; }
        [ForeignKey("ConclusionOfAuthorizedStateBody")]
        public int ConclusionOfAuthorizedStateBodyId { get; set; }
        public virtual LicenceProcesses LicenceProcesses { get; set; }
        [ForeignKey("LicenceProcesses")]
        public int LicenceProcessesId { get; set; }
        public virtual LicencePeriod LicencePeriod { get; set; }
        [ForeignKey("LicencePeriod")]
        public int LicencePeriodId { get; set; }
        public virtual InstitutionalLegalForm InstitutionalLegalForm { get; set; }
        [ForeignKey("InstitutionalLegalForm")]
        public int InstitutionalLegalFormId { get; set; }
        public virtual LicenceForm LicenceForm { get; set; }
        [ForeignKey("LicenceForm")]
        public int LicenceFormId { get; set; }
        public virtual TerritoryLocation TerritoryLocation { get; set; }
        [ForeignKey("TerritoryLocation")]
        public int TerritoryLocationId { get; set; }
        public virtual LicenceDenialReason LicenceDenialReason { get; set; }
        [ForeignKey("LicenceDenialReason")]
        public int LicenceDenialReasonId { get; set; }
        public virtual LicenceReissueReason LicenceReissueReason { get; set; }
        [ForeignKey("LicenceReissueReason")]
        public int LicenceReissueReasonId { get; set; }
        public virtual DuplicateReissueReason DuplicateReissueReason { get; set; }
        [ForeignKey("DuplicateReissueReason")]
        public int DuplicateReissueReasonId { get; set; }
        public virtual LicenceRenewal LicenceRenewal { get; set; }
        [ForeignKey("LicenceRenewal")]
        public int LicenceRenewalId { get; set; }
        public virtual ImpactMeasures ImpactMeasures { get; set; }
        [ForeignKey("ImpactMeasures")]
        public int ImpactMeasuresId { get; set; }

        public virtual Department Department { get; set; }
        [ForeignKey("Department")]
        public int DepartmentId { get; set; }
        public virtual LicenceAcceptance LicenceAcceptance { get; set; }
        [ForeignKey("LicenceAcceptance")]
        public int LicenceAcceptanceId { get; set; }

        public virtual ICollection<LicenceTracking> LicenceTracking { get; set; }

    }
}
