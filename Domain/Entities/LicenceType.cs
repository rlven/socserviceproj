﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class LicenceType : Entity<int>
    {
        public string Name { get; set; }
        public virtual Licence Licence { get; set; }
    }
}
