﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Country : Entity<int>
    {
        public string Name { get; set; }
        public virtual ICollection<LicenceAcceptance> LicenceAcceptanceAgency { get; set; }
        public virtual ICollection<LicenceAcceptance> LicenceAcceptanceLicencor { get; set; }
    }
}
