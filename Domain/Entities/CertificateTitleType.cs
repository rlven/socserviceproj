﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class CertificateTitleType : Entity<int>
    {
        public string TitleId { get; set; }
        public string TitleType { get; set; }

    }
}
