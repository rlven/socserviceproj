﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Domain.Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public string Position { get; set; }
        public string Signature { get; set; }

        public virtual Department Department { get; set; }
        [ForeignKey("Department")]
        public int DepartmentId { get; set; }
        public virtual ICollection<LicenceTracking> LicenceTracking { get; set; }
    }
}
