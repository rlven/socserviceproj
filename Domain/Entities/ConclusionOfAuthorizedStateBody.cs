﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class ConclusionOfAuthorizedStateBody : Entity<int>
    {
        public string Body { get; set; }
        public virtual Licence? Licence { get; set; }
    }
}
