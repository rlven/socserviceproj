﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class LicencePeriod : Entity<int>
    {
        public string Name { get; set; }
        public virtual Licence Licence { get; set; }
    }
}
