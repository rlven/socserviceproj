﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class ImpactMeasures : Entity<int>
    {
        public string Type { get; set; }
        public virtual Licence Licence { get; set; }
    }
}
