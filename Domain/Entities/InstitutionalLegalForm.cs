﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class InstitutionalLegalForm : Entity<int>
    {
        public string Title { get; set; }
        public virtual Licence Licence { get; set; }
    }
}
