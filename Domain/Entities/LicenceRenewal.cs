﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class LicenceRenewal : Entity<int>
    {
        public string ReasonName { get; set; }
        public int LicenceRenewalPeriod { get; set; }
        public virtual Licence Licence { get; set; }
    }
}
