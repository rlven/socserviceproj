﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class LicenceTracking : Entity<int>
    {
        public string Title { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Licence")]
        public int LicenceId { get; set; }
        public virtual Licence Licence { get; set; }
        public bool IsConfirmed { get; set; }
    }
}
