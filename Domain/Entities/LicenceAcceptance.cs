﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class LicenceAcceptance : Entity<int>
    {
        public string Name { get; set; }
        public virtual Country CountryLicensor { get; set; }
        [ForeignKey("CountryLicensor")]
        public int? CountryLicensorId { get; set; }
        public virtual Country CountryAgencyName { get; set; }
        [ForeignKey("CountryAgencyName")]
        public int? CountryAgencyNameId { get; set; }

        public string ForeignLicenceTermReview { get; set; }
        public string ForeignLicencePeriod { get; set; }
        public virtual Licence Licence { get; set; }
    }
}
