﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Department : Entity<int>
    {
        public string Name { get; set; }
        public virtual ICollection<Licence> Licences { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
