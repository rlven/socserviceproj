﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Interfaces.Repositories;

namespace Domain.Interfaces
{
    public interface IUnitOfWork
    {
        ILicenceRepository LicenceRepository { get; }
        IDepartmentRepository DepartmentRepository { get; }
        IUsersRepository UsersRepository { get; }
        ILicenceTrackingRepository LicenceTrackingRepository { get; }
        void Save();
        Task SaveAsync();
    }
}
