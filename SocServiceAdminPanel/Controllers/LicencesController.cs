﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using Infrastructure;

namespace SocServiceAdminPanel.Controllers
{
    public class LicencesController : Controller
    {
        private ILicenceService<Licence> _licenceService;
        private AppDbContext _context;

        public LicencesController(ILicenceService<Licence> licenceService, AppDbContext context)
        {
            _licenceService = licenceService;
            _context = context;
        }

        // GET: Licences
        public IActionResult Index()
        {
            var licences = _licenceService.GetAll();
            return View(licences.ToList());
        }

        // GET: Licences/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var licence = await _licenceService.GetByIdAsync((int)id);
            if (licence == null)
            {
                return NotFound();
            }

            return View(licence);
        }

        // GET: Licences/Create
        public IActionResult Create()
        {
            ViewBag.ConclusionOfAuthorizedStateBody = new SelectList(_context.ConclusionOfAuthorizedStateBodies, "Id", "Body");
            ViewBag.Department = new SelectList(_context.Departments, "Id", "Name");
            ViewBag.DuplicateReissueReason = new SelectList(_context.DuplicateReissueReasons, "Id", "Name");
            ViewBag.EducationalPrograms = new SelectList(_context.EducationalPrograms, "Id", "Name");
            ViewBag.ImpactMeasures = new SelectList(_context.ImpactMeasures, "Id", "Type");
            ViewBag.InstitutionalLegalForm = new SelectList(_context.InstitutionalLegalForms, "Id", "Title");
            ViewBag.LicenceAcceptance = new SelectList(_context.LicenceAcceptances, "Id", "Name");
            ViewBag.LicenceDenialReason = new SelectList(_context.LicenceDenialReasons, "Id", "Name");
            ViewBag.LicenceForm = new SelectList(_context.LicenceForms, "Id", "Name");
            ViewBag.LicencePeriod = new SelectList(_context.LicencePeriods, "Id", "Name");
            ViewBag.LicenceProcesses = new SelectList(_context.LicenceProcesses, "Id", "Name");
            ViewBag.LicenceReissueReason = new SelectList(_context.LicenceReissueReasons, "Id", "Name");
            ViewBag.LicenceRenewal = new SelectList(_context.LicenceRenewals, "Id", "ReasonName");
            ViewBag.LicenceTypes = new SelectList(_context.LicenceTypes, "Id", "Name");
            ViewBag.TerritoryLocation = new SelectList(_context.TerritoryLocations, "Id", "Name");

            return View();
        }

        // POST: Licences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LicenceNumber,TitleKg,TitleRu,TitleEn,TitleShort,LegalAddress,ActualAddress,TextpayerIdentityNumber,PhoneNumber,EmailAddress,CharterNumber,StateRegCertificate,LicenceDenialPeriod,LicenceDenialPeriodAuto,LicenceReissueReasonPeriod,LicenceDulicateIssuePeriod,IssueDate,DuplicateIssueDate,ReissueDate,LicenceAcceptanceDate,LicenceSuspensionDate,LicenceRenewalDate,LicenceCancellationDate,LicenceTypeId,EducationalProgramsId,ConclusionOfAuthorizedStateBodyId,LicenceProcessesId,LicencePeriodId,InstitutionalLegalFormId,LicenceFormId,TerritoryLocationId,LicenceDenialReasonId,LicenceReissueReasonId,DuplicateReissueReasonId,LicenceRenewalId,ImpactMeasuresId,DepartmentId,LicenceAcceptanceId,Id")] Licence licence)
        {
            if (ModelState.IsValid)
            {
                await _licenceService.CreateAsync(licence);
                
                return RedirectToAction(nameof(Index));
            }

            ViewBag.ConclusionOfAuthorizedStateBody = new SelectList(_context.ConclusionOfAuthorizedStateBodies, "Id", "Body");
            ViewBag.Department = new SelectList(_context.Departments, "Id", "Name");
            ViewBag.DuplicateReissueReason = new SelectList(_context.DuplicateReissueReasons, "Id", "Name");
            ViewBag.EducationalPrograms = new SelectList(_context.EducationalPrograms, "Id", "Name");
            ViewBag.ImpactMeasures = new SelectList(_context.ImpactMeasures, "Id", "Type");
            ViewBag.InstitutionalLegalForm = new SelectList(_context.InstitutionalLegalForms, "Id", "Title");
            ViewBag.LicenceAcceptance = new SelectList(_context.LicenceAcceptances, "Id", "Name");
            ViewBag.LicenceDenialReason = new SelectList(_context.LicenceDenialReasons, "Id", "Name");
            ViewBag.LicenceForm = new SelectList(_context.LicenceForms, "Id", "Name");
            ViewBag.LicencePeriod = new SelectList(_context.LicencePeriods, "Id", "Name");
            ViewBag.LicenceProcesses = new SelectList(_context.LicenceProcesses, "Id", "Name");
            ViewBag.LicenceReissueReason = new SelectList(_context.LicenceReissueReasons, "Id", "Name");
            ViewBag.LicenceRenewal = new SelectList(_context.LicenceRenewals, "Id", "ReasonName");
            ViewBag.LicenceTypes = new SelectList(_context.LicenceTypes, "Id", "Name");
            ViewBag.TerritoryLocation = new SelectList(_context.TerritoryLocations, "Id", "Name");
            return View(licence);
        }

        // GET: Licences/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var licence = await _licenceService.GetByIdAsync((int) id);
            if (licence == null)
            {
                return NotFound();
            }

            ViewBag.ConclusionOfAuthorizedStateBody = new SelectList(_context.ConclusionOfAuthorizedStateBodies, "Id", "Body");
            ViewBag.Department = new SelectList(_context.Departments, "Id", "Name");
            ViewBag.DuplicateReissueReason = new SelectList(_context.DuplicateReissueReasons, "Id", "Name");
            ViewBag.EducationalPrograms = new SelectList(_context.EducationalPrograms, "Id", "Name");
            ViewBag.ImpactMeasures = new SelectList(_context.ImpactMeasures, "Id", "Type");
            ViewBag.InstitutionalLegalForm = new SelectList(_context.InstitutionalLegalForms, "Id", "Title");
            ViewBag.LicenceAcceptance = new SelectList(_context.LicenceAcceptances, "Id", "Name");
            ViewBag.LicenceDenialReason = new SelectList(_context.LicenceDenialReasons, "Id", "Name");
            ViewBag.LicenceForm = new SelectList(_context.LicenceForms, "Id", "Name");
            ViewBag.LicencePeriod = new SelectList(_context.LicencePeriods, "Id", "Name");
            ViewBag.LicenceProcesses = new SelectList(_context.LicenceProcesses, "Id", "Name");
            ViewBag.LicenceReissueReason = new SelectList(_context.LicenceReissueReasons, "Id", "Name");
            ViewBag.LicenceRenewal = new SelectList(_context.LicenceRenewals, "Id", "ReasonName");
            ViewBag.LicenceTypes = new SelectList(_context.LicenceTypes, "Id", "Name");
            ViewBag.TerritoryLocation = new SelectList(_context.TerritoryLocations, "Id", "Name");
            return View(licence);
        }

        // POST: Licences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LicenceNumber,TitleKg,TitleRu,TitleEn,TitleShort,LegalAddress,ActualAddress,TextpayerIdentityNumber,PhoneNumber,EmailAddress,CharterNumber,StateRegCertificate,LicenceDenialPeriod,LicenceDenialPeriodAuto,LicenceReissueReasonPeriod,LicenceDulicateIssuePeriod,IssueDate,DuplicateIssueDate,ReissueDate,LicenceAcceptanceDate,LicenceSuspensionDate,LicenceRenewalDate,LicenceCancellationDate,LicenceTypeId,EducationalProgramsId,ConclusionOfAuthorizedStateBodyId,LicenceProcessesId,LicencePeriodId,InstitutionalLegalFormId,LicenceFormId,TerritoryLocationId,LicenceDenialReasonId,LicenceReissueReasonId,DuplicateReissueReasonId,LicenceRenewalId,ImpactMeasuresId,DepartmentId,LicenceAcceptanceId,Id")] Licence licence)
        {
            if (id != licence.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _licenceService.UpdateAsync(licence);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LicenceExists(licence.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewBag.ConclusionOfAuthorizedStateBody = new SelectList(_context.ConclusionOfAuthorizedStateBodies, "Id", "Body");
            ViewBag.Department = new SelectList(_context.Departments, "Id", "Name");
            ViewBag.DuplicateReissueReason = new SelectList(_context.DuplicateReissueReasons, "Id", "Name");
            ViewBag.EducationalPrograms = new SelectList(_context.EducationalPrograms, "Id", "Name");
            ViewBag.ImpactMeasures = new SelectList(_context.ImpactMeasures, "Id", "Type");
            ViewBag.InstitutionalLegalForm = new SelectList(_context.InstitutionalLegalForms, "Id", "Title");
            ViewBag.LicenceAcceptance = new SelectList(_context.LicenceAcceptances, "Id", "Name");
            ViewBag.LicenceDenialReason = new SelectList(_context.LicenceDenialReasons, "Id", "Name");
            ViewBag.LicenceForm = new SelectList(_context.LicenceForms, "Id", "Name");
            ViewBag.LicencePeriod = new SelectList(_context.LicencePeriods, "Id", "Name");
            ViewBag.LicenceProcesses = new SelectList(_context.LicenceProcesses, "Id", "Name");
            ViewBag.LicenceReissueReason = new SelectList(_context.LicenceReissueReasons, "Id", "Name");
            ViewBag.LicenceRenewal = new SelectList(_context.LicenceRenewals, "Id", "ReasonName");
            ViewBag.LicenceTypes = new SelectList(_context.LicenceTypes, "Id", "Name");
            ViewBag.TerritoryLocation = new SelectList(_context.TerritoryLocations, "Id", "Name");
            return View(licence);
        }

        // GET: Licences/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var licence = await _licenceService.GetByIdAsync((int) id);
            if (licence == null)
            {
                return NotFound();
            }

            return View(licence);
        }

        // POST: Licences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _licenceService.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        private bool LicenceExists(int id)
        {
            return _licenceService.GetAll().Any(e => e.Id == id);
        }
    }
}
