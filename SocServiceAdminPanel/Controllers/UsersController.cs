﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SocServiceAdminPanel.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService<User> _userService;
        private readonly AppDbContext _context;
        private readonly UserManager<User> _userManager;
        public UsersController(IUserService<User> userService, AppDbContext context, UserManager<User> userManager)
        {
            _userService = userService;
            _context = context;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View(_userService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Departments = new SelectList(_context.Departments, "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(User model)
        {
            if (ModelState.IsValid)
            {
                var employee = await _userService.CreateAsync(model);
                var result = await _userManager.CreateAsync(employee, Guid.NewGuid().ToString());
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Create));
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userService.GetByIdAsync(id);
            ViewBag.Departments = new SelectList(_context.Departments, "Id", "Name");
            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(User model, int departmentId)
        {
            if (ModelState.IsValid)
            {
                await _userService.UpdateAsync(model);
                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Edit));
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userService.GetByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _userService.GetByIdAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _userService.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}