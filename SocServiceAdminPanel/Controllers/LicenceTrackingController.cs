﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SocServiceAdminPanel.Controllers
{
    public class LicenceTrackingController : Controller
    {
        private readonly AppDbContext _context;
        private ILicenceTrackingService<LicenceTracking> _licenceTrackingService;

        public LicenceTrackingController(AppDbContext context, ILicenceTrackingService<LicenceTracking> licenceTrackingService)
        {
            _context = context;
            _licenceTrackingService = licenceTrackingService;
        }
        public IActionResult Index(string searchParam)
        {
            var requests = _licenceTrackingService.GetAll();
            if (!string.IsNullOrEmpty(searchParam))
            {
                requests = _licenceTrackingService.Search(requests, searchParam);
            }

            return View(requests);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Licences = new SelectList(_context.Licences, "Id", "LicenceNumber");
            ViewBag.Users = new SelectList(_context.Users, "Id", "FirstName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LicenceTracking model)
        {
            if (ModelState.IsValid)
            {
                await _licenceTrackingService.CreateAsync(model);

                return RedirectToAction(nameof(Index));
            }
            ViewBag.Licences = new SelectList(_context.Licences, "Id", "LicenceNumber");
            ViewBag.Users = new SelectList(_context.Users, "Id", "FirstName");

            return View(model);
        }

        public async Task<IActionResult> Aproove(int id)
        {
            await _licenceTrackingService.Aproove(id);
            return RedirectToAction(nameof(Index));
        }

    }
}