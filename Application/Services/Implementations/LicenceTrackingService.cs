﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;

namespace Application.Services.Implementations
{
    public class LicenceTrackingService : ILicenceTrackingService<LicenceTracking>
    {
        private readonly IUnitOfWork uow;

        public LicenceTrackingService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public List<LicenceTracking> GetAll()
        {
            return uow.LicenceTrackingRepository.All.ToList();
        }

        public async Task CreateAsync(LicenceTracking model)
        {
            var newLicenceTrack = new LicenceTracking()
            {
                Title = model.Title,
                UserId = model.UserId,
                LicenceId = model.LicenceId,
                IsConfirmed = false
            };
            await uow.LicenceTrackingRepository.AddAsync(newLicenceTrack);
            await uow.SaveAsync();
        }

        public async Task Aproove(int licenceTrackId)
        {
            var request = await uow.LicenceTrackingRepository.GetByIdAsync(licenceTrackId);
            request.IsConfirmed = true;
            await uow.SaveAsync();
        }

        public List<LicenceTracking> Search(List<LicenceTracking> data, string searchParam)
        {
            return data.Where(x => x.Title.Contains(searchParam)).ToList();
        }
    }
}
