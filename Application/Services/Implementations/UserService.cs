﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Application.Services.Implementations
{
    public class UserService : IUserService<User>
    {
        private readonly IUnitOfWork uow;

        public UserService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public List<User> GetAll()
        {
            return uow.UsersRepository.All.ToList();
        }

        public async Task<User> CreateAsync(User model)
        {
            var newUser = new User()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                FatherName = model.FatherName,
                Email = model.Email,
                UserName = model.Email,
                PhoneNumber = model.FirstName,
                DepartmentId = model.DepartmentId
            };
            return newUser;
        }

        public async Task UpdateAsync(User model)
        {
            var user = uow.UsersRepository.GetById(model.Id);
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.FatherName = model.FatherName;
            user.DepartmentId = model.DepartmentId;
            uow.UsersRepository.Update(user);
            await uow.SaveAsync();
        }

        public async Task<User> GetByIdAsync(string id)
        {
            var user = uow.UsersRepository.GetById(id);
            return user;
        }

        public async Task DeleteAsync(string id)
        {
            await uow.UsersRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }
    }
}
