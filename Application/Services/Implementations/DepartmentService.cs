﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;

namespace Application.Services.Implementations
{
    public class DepartmentService : IDepartmentService<Department>
    {
        private readonly IUnitOfWork uow;

        public DepartmentService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public List<Department> GetAll()
        {
            return uow.DepartmentRepository.All.ToList();
        }

        public async Task<Department> CreateAsync(Department model)
        {
            await uow.DepartmentRepository.AddAsync(model);
            await uow.SaveAsync();
            return model;
        }

        public async Task UpdateAsync(Department model)
        {
            uow.DepartmentRepository.Update(model);
            await uow.SaveAsync();
        }

        public async Task<Department> GetByIdAsync(int id)
        {
            return await uow.DepartmentRepository.GetByIdAsync(id);
        }

        public async Task DeleteAsync(int id)
        {
            await uow.DepartmentRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }
    }
}
