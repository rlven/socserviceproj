﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Application.Services.Implementations
{
    public class LicenceService : ILicenceService<Licence>
    {
        private readonly IUnitOfWork uow;

        public LicenceService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public List<Licence> GetAll()
        {
            return uow.LicenceRepository.All.ToList();
        }

        public async Task<Licence> CreateAsync(Licence model)
        {
            await uow.LicenceRepository.AddAsync(model);
            await uow.SaveAsync();
            return model;
        }

        public async Task UpdateAsync(Licence model)
        {
            uow.LicenceRepository.Update(model);
            await uow.SaveAsync();
        }

        public async Task<Licence> GetByIdAsync(int id)
        {
            return await uow.LicenceRepository.GetByIdAsync(id);
        }

        public async Task DeleteAsync(int id)
        {
            await uow.LicenceRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }
    }
}
