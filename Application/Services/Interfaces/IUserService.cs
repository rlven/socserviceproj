﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Application.Services.Interfaces
{
    public interface IUserService<T> where T : class
    {
        List<User> GetAll();
        Task<User> CreateAsync(T model);
        Task UpdateAsync(T model);
        Task<User> GetByIdAsync(string id);
        Task DeleteAsync(string id);
    }
}
