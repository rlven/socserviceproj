﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Application.Services.Interfaces
{
    public interface ILicenceService<T> where T : class
    {
        List<Licence> GetAll();
        Task<Licence> CreateAsync(T model);
        Task UpdateAsync(T model);
        Task<Licence> GetByIdAsync(int id);
        Task DeleteAsync(int id);
    }
}
