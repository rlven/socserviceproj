﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Application.Services.Interfaces
{
    public interface IDepartmentService<T> where T : class
    {
        List<Department> GetAll();
        Task<Department> CreateAsync(T model);
        Task UpdateAsync(T model);
        Task<Department> GetByIdAsync(int id);
        Task DeleteAsync(int id);
    }
}
