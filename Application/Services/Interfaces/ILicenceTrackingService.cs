﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Application.Services.Interfaces
{
    public interface ILicenceTrackingService<T> where T : class
    {
        List<LicenceTracking> GetAll();
        Task CreateAsync(T model);
        Task Aproove(int licenceTrackId);
        List<LicenceTracking> Search(List<LicenceTracking> data, string searchParam);
    }
}
